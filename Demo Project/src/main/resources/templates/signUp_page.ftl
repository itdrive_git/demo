<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/signUp.css" rel="stylesheet">
    <title>Регистрация</title>
</head>
<body>
<form class="form-signin" action="/users" method="post">

    <div class="text-center mb-4">
        <img class="mb-4" src="/images/itdrive.png" alt="" width="140">
    </div>

    <div class="form-label-group">
        <input id="inputEmail" class="form-control" name="email" placeholder="Email">
    </div>

    <div class="form-label-group">
        <input name="password" id="inputPassword" class="form-control" type="password" placeholder="Password">
    </div>

    <input class="btn btn-lg btn-primary btn-block" type="submit" value="Sign Up">
</form>
</body>
</html>