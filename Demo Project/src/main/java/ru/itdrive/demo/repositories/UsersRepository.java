package ru.itdrive.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itdrive.demo.models.User;

public interface UsersRepository extends JpaRepository<User, Long> {
}
