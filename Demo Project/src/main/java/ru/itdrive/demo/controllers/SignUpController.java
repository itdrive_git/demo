package ru.itdrive.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/signUp")
public class SignUpController {
    @GetMapping
    public String getSignUpPage() {
        return "signUp_page";
    }
}
